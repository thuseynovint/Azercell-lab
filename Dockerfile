FROM openjdk:11-jdk
WORKDIR /app

COPY HelloWorldProducer.java /app
COPY pom.xml /app
RUN apt-get update && apt-get install -y maven
RUN mvn dependency:copy-dependencies

RUN javac -cp "/app/target/dependency/*" HelloWorldProducer.java

CMD ["java", "-cp", "/app:/app/target/dependency/*", "HelloWorldProducer"]





