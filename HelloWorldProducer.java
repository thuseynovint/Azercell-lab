import org.apache.kafka.clients.producer.*;
import java.util.Properties;

public class HelloWorldProducer {
    public static void main(String[] args) {
        String topicName = "hello-topic";
        String bootstrapServers = "10.100.35.45:9092"; // Kafka broker address within the Docker network

        Properties props = new Properties();
        props.put("bootstrap.servers", bootstrapServers);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");

        Producer<String, String> producer = new KafkaProducer<>(props);

        try {
            while (true) {
                String message = "Hello, World!";
                ProducerRecord<String, String> record = new ProducerRecord<>(topicName, message);
                producer.send(record, new Callback() {
                    public void onCompletion(RecordMetadata metadata, Exception exception) {
                        if (exception != null) {
                            exception.printStackTrace();
                        } else {
                            System.out.println("Sent message: " + message);
                        }
                    }
                });
                
                // Sleep for 2 seconds before sending the next message
                Thread.sleep(2000);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            producer.close();
        }
    }
}

